import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:weather_app/globalVariable.dart';
import 'package:weather_app/util/colors.dart';

import '../models/weather_locations.dart';
import '../widgets/single_weather.dart';

class WeatherApp extends StatefulWidget {
  const WeatherApp(
      {super.key,
      required this.cityName,
      required this.state,
      required this.country});
  final String cityName;
  final String state;
  final String country;
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  int _currentPage = 0;
  String bgImg = "assets/sunny.jpg";
  WeatherLocation? cityWeather;
  bool _isLoading = true;
  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  void initState() {
    super.initState();
    getcitydata();
  }

  Future<void> getcitydata() async {
    final response = await http.get(
      Uri.parse(
          'http://api.weatherapi.com/v1/current.json?key=674a347f7b474aa7b7581826230506&q=${widget.cityName}'),
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      print("Success");
      Map<String, dynamic> res = jsonDecode(response.body);

      print("https:${res["current"]["condition"]["icon"]}");
      setState(() {
        cityWeather = WeatherLocation(
            city: res["location"]["name"],
            dateTime: res["location"]["localtime"],
            temparature: res["current"]["temp_c"].toString(),
            weatherType: res["current"]["condition"]["text"].toString(),
            iconUrl: "https:${res["current"]["condition"]["icon"]}",
            wind: res["current"]["wind_kph"],
            rain: res["current"]["cloud"],
            humidity: res["current"]["humidity"]);
        _isLoading = false;
      });
      // return WeatherLocation(city: re, dateTime: dateTime, temparature: temparature, weatherType: weatherType, iconUrl: iconUrl, wind: wind, rain: rain, humidity: humidity);
    } else {
      setState(() {
        _isLoading = false;
      });
      showSnackbar(
          context,
          "Some unknown error has occur status code ${response.statusCode}",
          colorError);
      print("Fail");
      // return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // extendBodyBehindAppBar: true,
      // appBar: AppBar(
      //   title: Text(''),
      //   backgroundColor: Colors.transparent,
      //   elevation: 0,
      // ),
      body: Container(
        child: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Stack(
                children: [
                  // Image.asset(
                  //   bgImg,
                  //   fit: BoxFit.cover,
                  //   height: double.infinity,
                  //   width: double.infinity,
                  // ),
                  // Container(
                  //   decoration: BoxDecoration(color: Colors.black38),
                  // ),
                  if (cityWeather != null)
                    SingleWeather(cityWeather!, widget.state, widget.country),
                ],
              ),
      ),
    );
  }
}
