import 'package:flutter/material.dart';
import 'package:searchfield/searchfield.dart';
import 'package:weather_app/Api/database_api.dart';
import 'package:weather_app/globalVariable.dart';
import 'package:weather_app/screens/weather_app.dart';
import 'package:weather_app/util/colors.dart';
import 'package:weather_app/util/constants.dart';
import 'package:weather_app/util/text_styles.dart';
import 'package:weather_app/widgets/new_button.dart';
import 'package:weather_app/widgets/splash_screen.dart';

class selectionpage extends StatefulWidget {
  const selectionpage({super.key});

  @override
  State<selectionpage> createState() => _selectionpageState();
}

class _selectionpageState extends State<selectionpage> {
  String bgImg = "assets/sunny.jpg";
  bool _isLoading = true;
  bool _isCompleteForm = false;
  List<dynamic> _countryData = [];
  List<String> _countryName = [];
  List<String> _stateName = [];
  List<String> _cityNameList = [];
  final TextEditingController _country = TextEditingController();
  final TextEditingController _city = TextEditingController();
  final TextEditingController _state = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool splash = true;

  @override
  void initState() {
    super.initState();
    initFun();
  }

  @override
  Widget build(BuildContext context) {
    return splash
        ? const SplaceScreen()
        : WillPopScope(
            onWillPop: () {
              AppExitPopup(context);
              return Future.value(false);
            },
            child: Scaffold(
              body: _isLoading
                  ? const Center(child: CircularProgressIndicator())
                  : Center(
                      child: Form(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: Column(
                          children: [
                            Container(
                              margin: const EdgeInsets.symmetric(
                                  horizontal: constants.defaultPadding),
                              decoration: BoxDecoration(
                                borderRadius: constants.borderRadius,
                                gradient: const LinearGradient(
                                    colors: [
                                      Color(0xFFF2D29B),
                                      Color(0xFFF0BF4D),
                                    ],
                                    begin: FractionalOffset(0, -1),
                                    end: FractionalOffset(-1, 1),
                                    stops: [0.0, 1.0],
                                    tileMode: TileMode.clamp),
                              ),
                              padding: EdgeInsets.all(24),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const SizedBox(
                                    height: 111,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: constants.defaultPadding),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Enter details",
                                        style: textStyle.heading.copyWith(
                                            color: colorWhite, fontSize: 33),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: constants.defaultPadding * 2,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: constants.borderRadius,
                                    ),
                                    padding: const EdgeInsets.all(8.0),
                                    child: SearchField(
                                      controller: _country,
                                      suggestionAction:
                                          SuggestionAction.unfocus,
                                      suggestions: _countryName
                                          .map(
                                            (e) => SearchFieldListItem<String>(
                                              e,
                                              item: e,
                                            ),
                                          )
                                          .toList(),
                                      searchInputDecoration: InputDecoration(
                                        labelStyle: const TextStyle(
                                            color: Colors.white),
                                        labelText: "Select Country",
                                        border: OutlineInputBorder(
                                          borderRadius: constants.borderRadius,
                                          borderSide: const BorderSide(
                                              color: colorWhite),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: colorWhite),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: Colors.red),
                                        ),
                                      ),
                                      validator: ((value) {
                                        if (value == null || value.isEmpty) {
                                          return "Please Enter Country";
                                        }
                                        return null;
                                      }),
                                      onSuggestionTap: (val) {
                                        print(val.item);
                                        setState(() {
                                          _country.text = val.item.toString();
                                        });
                                        bool check =
                                            _countryName.contains(val.item);
                                        print(check);
                                        if (check == true) {
                                          print("contains");
                                          int index = _countryName
                                              .indexOf(val.item.toString());
                                          print(index);
                                          Api()
                                              .getstate(val.item.toString())
                                              .then((value) {
                                            _stateName = value;
                                            setState(() {});
                                          });

                                          print(
                                              "final state list ::  $_stateName");
                                        }
                                      },
                                      maxSuggestionsInViewPort: 6,
                                      itemHeight: 50,
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: constants.borderRadius,
                                    ),
                                    padding: const EdgeInsets.all(8.0),
                                    child: SearchField(
                                      controller: _state,
                                      suggestionAction:
                                          SuggestionAction.unfocus,
                                      suggestions: _stateName
                                          .map(
                                            (e) => SearchFieldListItem<String>(
                                              e,
                                              item: e,
                                            ),
                                          )
                                          .toList(),
                                      searchInputDecoration: InputDecoration(
                                        labelStyle: const TextStyle(
                                            color: Colors.white),
                                        labelText: "Select State",
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: colorWhite),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: colorWhite),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: Colors.red),
                                        ),
                                      ),
                                      validator: ((value) {
                                        if (value == null || value.isEmpty) {
                                          return "Please Enter State";
                                        }
                                        return null;
                                      }),
                                      onSuggestionTap: (val) {
                                        print(val.item);
                                        setState(() {
                                          _state.text = val.item.toString();
                                        });
                                        bool check =
                                            _stateName.contains(val.item);
                                        print(check);
                                        if (check == true) {
                                          print("contains");
                                          Api()
                                              .getcity(
                                                  _country.text, _state.text)
                                              .then((value) {
                                            setState(() {
                                              _cityNameList = value;
                                            });
                                          });

                                          print(
                                              "final city list ::  $_stateName");
                                        }
                                      },
                                      maxSuggestionsInViewPort: 6,
                                      itemHeight: 50,
                                    ),
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0)),
                                    padding: const EdgeInsets.all(8.0),
                                    child: SearchField(
                                      controller: _city,
                                      suggestionAction:
                                          SuggestionAction.unfocus,
                                      suggestions: _cityNameList
                                          .map(
                                            (e) => SearchFieldListItem<String>(
                                              e,
                                              item: e,
                                            ),
                                          )
                                          .toList(),
                                      searchInputDecoration: InputDecoration(
                                        labelStyle: const TextStyle(
                                            color: Colors.white),
                                        labelText: "Select City",
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: colorWhite),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: colorWhite),
                                        ),
                                        errorBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                          borderSide: const BorderSide(
                                              color: Colors.red),
                                        ),
                                      ),
                                      validator: ((value) {
                                        if (value == null || value.isEmpty) {
                                          return "Please Enter City";
                                        }
                                        return null;
                                      }),
                                      onSuggestionTap: (val) {
                                        print(val.item);
                                        setState(() {
                                          _city.text = val.item.toString();
                                        });
                                        bool check =
                                            _stateName.contains(val.item);
                                        print(check);
                                        if (check == true) {
                                          // print("contains");
                                          // Api()
                                          //     .getcity(_country.text, _state.text)
                                          //     .then((value) {
                                          //   _cityNameList = value;
                                          // });
                                          //
                                          // print("final city list ::  $_stateName");
                                        }
                                      },
                                      maxSuggestionsInViewPort: 6,
                                      itemHeight: 50,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: constants.defaultPadding * 2,
                            ),
                            NewButton(
                              height: 55,
                              context: context,
                              buttonText: "Submit",
                              textStyle:
                                  textStyle.button.copyWith(fontSize: 16),
                              function: () async {
                                if (!await checkInternet()) {
                                  showSnackbar(context,
                                      "No internet connection", colorError);
                                  return;
                                }
                                if (_formKey.currentState!.validate()) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => WeatherApp(
                                        cityName: _city.text.trim(),
                                        state: _state.text.trim(),
                                        country: _country.text.trim(),
                                      ),
                                    ),
                                  ).then((value) {
                                    _country.clear();
                                    _state.clear();
                                    _city.clear();
                                  });
                                }
                              },
                              borderRadius: constants.borderRadius,
                              margin: EdgeInsets.all(12),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          );
  }

  Future<void> initFun() async {
    if (await checkInternet()) {
      Api().getcountry().then((value) {
        _countryData = value;
        for (var i in _countryData) {
          _countryName.add(i["country"]);
        }
        print("Country List :: $_countryName");
        // delay(1000).then((value) {
        setState(() {
          _isLoading = false;
          splash = false;
          // });
        });
      });
    } else {
      await delay(2000).then((value) {
        setState(() {
          _isLoading = false;
          splash = false;
          // });
        });
        showSnackbar(context, "No internet connection", colorError);
      });
    }
  }
}
