import 'package:flutter/material.dart';
import 'package:weather_app/util/colors.dart';
import 'package:weather_app/util/constants.dart';
import 'package:weather_app/util/text_styles.dart' as dev;

class NewButton extends StatelessWidget {
  final BuildContext context;
  final String buttonText;
  final double width;
  final double height;
  final Function function;
  Color color;
  bool border = false;
  bool borderColorDiff;
  final Widget child;
  final bool rDefaultWidget;
  final TextStyle? textStyle;
  final margin;
  final borderRadius;
  final Color borderSide;
  final bool enable;

  NewButton({
    required this.context,
    this.buttonText = "Text",
    required this.function,
    this.border = false,
    this.width = double.infinity,
    this.height = 46,
    this.color = colorDark,
    this.borderColorDiff = false,
    this.rDefaultWidget = true,
    this.child = const Text("default"),
    this.textStyle,
    this.margin,
    this.borderRadius,
    this.borderSide = colorWhite,
    this.enable = true,
  });

  @override
  Widget build(BuildContext context) {
    if (!enable) {
      color = colorDisable;
    }
    return Padding(
      padding: margin ?? EdgeInsets.zero,
      child: SizedBox(
        height: height,
        width: width,
        child: TextButton(
          onPressed: enable ? () => function() : null,
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.resolveWith(
              (states) {
                return states.contains(MaterialState.pressed)
                    ? border
                        ? color.withOpacity(0.1)
                        : Colors.white.withOpacity(0.3)
                    : null;
              },
            ),
            backgroundColor: border
                ? MaterialStateProperty.all(colorWhite)
                : MaterialStateProperty.all(color),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: borderRadius ?? constants.borderRadius * 3,
                side: border || borderColorDiff
                    ? BorderSide(
                        color: borderColorDiff ? borderSide : color, width: 1.5)
                    : BorderSide.none,
              ),
            ),
          ),
          child: rDefaultWidget
              ? Center(
                  child: Text(
                  buttonText,
                  style: textStyle ??
                      dev.textStyle.button.copyWith(color: colorWhite),
                ))
              : child,
        ),
      ),
    );
  }
}
