import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:weather_app/models/weather_locations.dart';
import 'package:weather_app/util/colors.dart';
import 'package:weather_app/util/constants.dart';
import 'package:weather_app/widgets/back_button.dart';

class SingleWeather extends StatelessWidget {
  final WeatherLocation cityweather;
  const SingleWeather(this.cityweather, this.state, this.country);

  final String state;
  final String country;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Container(
            margin: const EdgeInsets.symmetric(
                horizontal: constants.defaultPadding),
            decoration: BoxDecoration(
              borderRadius: constants.borderRadius,
              gradient: const LinearGradient(
                  colors: [
                    Color(0xFFF2D29B),
                    Color(0xFFF0BF4D),
                  ],
                  begin: FractionalOffset(0, -1),
                  end: FractionalOffset(-1, 1),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            padding: const EdgeInsets.all(24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: constants.defaultPadding * 3,
                    ),
                    const MyBackButton(
                      padding: EdgeInsets.zero,
                    ),
                    const SizedBox(
                      height: 55,
                    ),
                    Text(
                      "$country, $state",
                      style: GoogleFonts.lato(
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 22,
                    ),
                    Text(
                      cityweather.city,
                      style: GoogleFonts.lato(
                        fontSize: 44,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      cityweather.dateTime,
                      style: GoogleFonts.lato(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${cityweather.temparature}°C",
                      style: GoogleFonts.lato(
                        fontSize: 44,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    Row(
                      children: [
                        Image.network(cityweather.iconUrl),
                        // SvgPicture.asset(
                        //   cityweather.iconUrl,
                        //   width: 34,
                        //   height: 34,
                        //   color: Colors.white,
                        // ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          cityweather.weatherType,
                          style: GoogleFonts.lato(
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Text(
                    'Wind',
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Text(
                    cityweather.wind.toString(),
                    style: GoogleFonts.lato(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Text(
                    'km/h',
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 5,
                        width: 50,
                        color: colorHeadingText,
                      ),
                      Container(
                        height: 5,
                        width: cityweather.wind / 2,
                        color: Colors.greenAccent,
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    'Rain',
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Text(
                    cityweather.rain.toString(),
                    style: GoogleFonts.lato(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Text(
                    '%',
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 5,
                        width: 50,
                        color: colorHeadingText,
                      ),
                      Container(
                        height: 5,
                        width: cityweather.rain / 2,
                        color: Colors.redAccent,
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    'Humidy',
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Text(
                    cityweather.humidity.toString(),
                    style: GoogleFonts.lato(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Text(
                    '%',
                    style: GoogleFonts.lato(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: colorHeadingText,
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 5,
                        width: 50,
                        color: colorHeadingText,
                      ),
                      Container(
                        height: 5,
                        width: cityweather.humidity / 2,
                        color: Colors.redAccent,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
