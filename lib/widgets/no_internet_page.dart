import 'dart:async';

import 'package:flutter/material.dart';
import 'package:weather_app/util/constants.dart';
import 'package:weather_app/util/text_styles.dart';

import '../globalVariable.dart';
import '../util/colors.dart';

class NoInternetPage extends StatefulWidget {
  const NoInternetPage({Key? key}) : super(key: key);

  @override
  _NoInternetPageState createState() => _NoInternetPageState();
}

class _NoInternetPageState extends State<NoInternetPage> {
  late Timer timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    timer = Timer.periodic(const Duration(seconds: 5), (timer) async {
      if (await checkInternet()) {
        timer.cancel();
        // nextPage(context, const HomePage());
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () {
          AppExitPopup(context);
          return Future.value(false);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(
              Icons.wifi_off_rounded,
              color: colorWarning,
              size: 122,
            ),
            const SizedBox(
              height: constants.defaultPadding * 2,
            ),
            Center(
              child: Text(
                "No internet connection !",
                style: textStyle.heading,
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: constants.defaultPadding / 2,
            ),
            Center(
              child: Text(
                "You are offline, please enable mobile data or connect thought wifi.",
                style: textStyle.subHeadingColorDark
                    .copyWith(color: colorSubHeadingText),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
