import 'package:flutter/material.dart';
import 'package:weather_app/util/colors.dart';
import 'package:weather_app/util/constants.dart';
import 'package:weather_app/util/text_styles.dart';

class SplaceScreen extends StatelessWidget {
  const SplaceScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              "assets/logo.png",
              width: 111,
              height: 111,
              // fit: BoxFit.fill,
              // height: double.infinity,
              // width: double.infinity,
            ),
            const SizedBox(
              height: constants.defaultPadding,
            ),
            Text(
              "Whether App",
              style: textStyle.heading
                  .copyWith(color: colorHeadingText, fontSize: 22),
            )
          ],
        ),
      ),
    );
  }
}
