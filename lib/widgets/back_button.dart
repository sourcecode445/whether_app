import 'package:flutter/material.dart';
import 'package:weather_app/util/colors.dart';
import 'package:weather_app/util/constants.dart';

class MyBackButton extends StatelessWidget {
  const MyBackButton({
    Key? key,
    this.padding,
    this.onPress,
    this.color,
    this.height = 0,
    this.width = 0,
  }) : super(key: key);

  final padding;
  final color;
  final height;
  final width;
  final Function? onPress;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onPress == null) {
          Navigator.pop(context);
          return;
        }
        onPress!();
      },
      child: Padding(
        padding: padding ?? const EdgeInsets.all(constants.defaultPadding),
        child: Align(
          alignment: Alignment.topLeft,
          child: Icon(
            Icons.arrow_back_rounded,
            size: 26,
            color: colorWhite,
          ),
          // child: Image.asset('images/backbtn.png',height: height ==0?  44.h : height ,width: width ==0?  44.h : width,color: color,)
        ),
      ),
    );
  }
}
