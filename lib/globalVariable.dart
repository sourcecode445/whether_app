library my_prj.globals;

import 'dart:io';

import 'package:flutter/material.dart';

//=========================Variables============================================
Future<void> delay(int time) async {
  await Future.delayed(Duration(milliseconds: time));
}

void showSnackbar(context, String message, color, [int duration = 4000]) {
  try {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
  } catch (e) {
    // customPrint("SnackBar Error :: $e");
  }
  final snackBar = SnackBar(
    elevation: 6.0,
    behavior: SnackBarBehavior.floating,
    margin: const EdgeInsets.all(12),
    backgroundColor: color,
    duration: Duration(milliseconds: duration),
    content: Text(message),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

Future<bool> checkInternet() async {
  try {
    return await InternetAddress.lookup('www.google.com').then((result) {
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      }
      return false;
    });
  } on SocketException catch (_) {
    print('not connected');
    return false;
  }
}

void AppExitPopup(BuildContext context,
    [String title = "Exit", String subTitle = "Do you really want to exit ?"]) {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text(title),
      content: Text(subTitle),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: const Text('No'),
        ),
        TextButton(
          onPressed: () => exit(0),
          child: const Text('Yes'),
        ),
      ],
    ),
  );
}
