import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Api {
  Future<List<dynamic>> getcountry() async {
    final response = await http.get(
      Uri.parse('https://countriesnow.space/api/v0.1/countries'),
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      print("Success");
      Map<String, dynamic> res = jsonDecode(response.body);
      List<dynamic> countryList = res["data"];
      print(countryList);
      return countryList;
    } else {
      print("Fail");
      return [];
    }
  }

  Future<List<String>> getstate(String _country) async {
    print("getstate country :: $_country");
    final response = await http.get(
      Uri.parse(
          'https://countriesnow.space/api/v0.1/countries/states/q?country=$_country'),
    );
    print("getstate :: statusCode${response.statusCode}");
    print("getstate :: body${response.body}");
    if (response.statusCode == 200) {
      print("Success");
      Map<String, dynamic> res = jsonDecode(response.body.toString());
      List<dynamic> stateList = res["data"]["states"];
      print("State List :: " + stateList.toString());
      List<String> stateListFinal = [];
      for (var i in stateList) {
        stateListFinal.add(i["name"]);
      }
      return stateListFinal;
    } else {
      print("Fail");
      return [];
    }
  }

  Future<List<String>> getcity(String _country, String _state) async {
    print("getcity country :: $_country");
    print("getcity state :: $_state");
    final response = await http.get(
      Uri.parse(
          'https://countriesnow.space/api/v0.1/countries/state/cities/q?country=$_country&state=$_state'),
    );
    print("getcity :: statusCode${response.statusCode}");
    print("getcity :: body${response.body}");
    if (response.statusCode == 200) {
      print("Success");
      Map<String, dynamic> res = jsonDecode(response.body.toString());
      List<dynamic> cityList = res["data"];
      List<String> cityListFinal = [];
      for (var i in cityList) {
        cityListFinal.add(i);
      }
      return cityListFinal;
    } else {
      print("Fail");
      return [];
    }
  }
}
