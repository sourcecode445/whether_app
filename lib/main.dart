import 'package:flutter/material.dart';
import 'package:weather_app/screens/selection_screen.dart';
import 'package:weather_app/util/colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: colorCustom,
        fontFamily: "poppins",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: selectionpage(),
    );
  }
}
