library my_prj.globals;

import 'package:flutter/material.dart';

import 'colors.dart';

class textStyle {
  ///heading
  static final TextStyle heading =
      TextStyle(color: colorDark, fontWeight: FontWeight.bold, fontSize: 20);

  static final TextStyle bigHeading =
      TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25);

  static final TextStyle headingReg = TextStyle(color: colorDark, fontSize: 20);

  static final TextStyle headingsemibold = TextStyle(
      color: colorDark,
      fontWeight: FontWeight.w700,
      fontSize: 20,
      fontFamily: 'poppinssemibold');

  static final TextStyle headingLight = TextStyle(
      color: colorDark,
      // fontWeight: FontWeight.w700,
      fontSize: 20,
      fontFamily: 'poppinslight');

  ///subheading
  static final TextStyle subHeading = TextStyle(
      color: colorSubHeadingText, fontWeight: FontWeight.normal, fontSize: 16);

  static final TextStyle subHeadingSemibold = TextStyle(
      color: colorSubHeadingText,
      fontFamily: 'poppinssemibold',
      fontWeight: FontWeight.normal,
      fontSize: 16);

  static final TextStyle subHeadingSemibold2 = TextStyle(
      color: colorSubHeadingText,
      fontFamily: 'poppinssemibold',
      fontWeight: FontWeight.normal,
      fontSize: 16);

  static final TextStyle subHeadingLigh = TextStyle(
      color: colorSubHeadingText,
      fontFamily: 'poppinslight',
      fontWeight: FontWeight.normal,
      fontSize: 16);

  static final TextStyle subHeadingColorDark =
      subHeading.copyWith(color: colorHeadingText, fontWeight: FontWeight.bold);

  ///small text
  static final TextStyle smallTextColorDark =
      TextStyle(color: colorDark, fontWeight: FontWeight.bold, fontSize: 12);

  static final TextStyle smallTextSemiBoldDark = TextStyle(
      color: colorSubHeadingText,
      fontFamily: 'poppinssemibold',
      fontWeight: FontWeight.w500,
      fontSize: 13);

  static final TextStyle smallText = TextStyle(
      color: colorSubHeadingText, fontWeight: FontWeight.normal, fontSize: 13);

  static final TextStyle smallTextSemiBold = TextStyle(
      color: colorSubHeadingText,
      fontFamily: 'poppinssemibold',
      fontWeight: FontWeight.w300,
      fontSize: 13);

  static final TextStyle smallTextLight = TextStyle(
      color: colorSubHeadingText,
      fontFamily: 'poppinslight',
      fontWeight: FontWeight.normal,
      fontSize: 13);

  static final TextStyle button =
      TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14);

  static final TextStyle subButton = TextStyle(
      color: Colors.white, fontWeight: FontWeight.normal, fontSize: 14);
}
