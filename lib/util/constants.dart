library my_prj.globals;

import 'package:flutter/material.dart';

class constants {
  static double radius = 15;
  // static double buttonRadius = 28.r;
  static double thickness = 2;
  static final borderRadius = BorderRadius.circular(radius);
  // static final buttonBorderRadius = BorderRadius.circular(buttonRadius);
  static const defaultPadding = 12.0;
  static String flutterLongText =
      "Miusov, as a man man of breeding and deilcacy, could "
      "not but feel some inwrd qualms, when he reached the Father Superior's with "
      "Ivan: he felt ashamed of havin lost his temper. He felt that he ought to "
      "have disdaimed that despicable wretch, Fyodor Pavlovitch, too much to have "
      "been upset by him in Father Zossima's cell, and so to have "
      "forgotten himself.";
  static String flutterShortText =
      "Note that UltimateSpell displays the text in the "
      "dialog box sentence-by-sentence just like Microsoft Word.";
}

String folderBasePath = "";
